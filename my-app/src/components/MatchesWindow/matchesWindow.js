import React, {Component} from 'react';
import Topic from "../Topic/topic";
import "./css/style.scss";

class MatchesWindow extends Component {
    render() {
        const data=[
            {
                key:1,
                pic:"./3.jpeg",
                name:"Christin Jhonatan"
            },
            {
                key:2,
                pic:"./1.jfif",
                name:"Anna Joseph"
            },
            {
                key:3,
                pic:"./6.jpg",
                name:"Julia Cohen"
            },

        ];
        return (
            <div className={"matches"} >
                <p className={"matches__text"}>Matches</p>
                {console.log(data)}
                {data.map(topic=>
                    <Topic image={topic.pic} key={topic.key} name={topic.name}/>
                )}
            </div>
        );
    }
}

export default MatchesWindow;