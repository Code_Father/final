import React, {Component} from 'react';
import "./css/style.scss";

class UserInfo extends Component {
    render() {
        return (
            <div className={"user"}>
                <img src="./3.jpeg"
                     className={"user__msg-info-img"}
                     alt="img"/>
                     <h2 className={"user__msg-info-header"}>Christin Jhonatan, 22</h2>
                <p className={"user__msg-info-text"}>christina97@gmail.com</p>
            </div>
        );
    }
}

export default UserInfo;