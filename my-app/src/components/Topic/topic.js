import React, {Component} from 'react';
import "./css/style.scss";

class Topic extends Component {
    render() {
        return (
            <div className={"topic"}>

                    <img className={"topic__img-container"} src={this.props.image} alt="image"/>

                <h4 className={"topic__header"}>{this.props.name}</h4>
                {this.props.name!=="Christin Jhonatan"? <h4 style={{color: "red", marginLeft: "25px"}}>new</h4>:null}
            </div>
        );
    }
}

export default Topic;