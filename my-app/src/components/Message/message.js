import React, {Component} from 'react';
import Header from "../Header/header";
import ChatAppContainer from "../ChatAppContainer/chatAppContainer";
import MessageInput from "../MessageInput/messageInput";
import "./css/style.scss";

class Message extends Component {

    render() {
        const data={
            Rauf: {
                info:{
                    pics: [
                        "https://images.unsplash.com/photo-1520024146169-3240400354ae?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                    ],
                    name: "Rauf Ismayilov",
                    age: 32,
                    distance: "4 miles away",
                    text: "bla bla bla bla bla bla"
                },
                messages:[
                    {from: 'Rauf',msg: 'Hey'},
                    {from: 'me',msg: 'Hi'},
                    {from: 'Rauf',msg: 'What r u up to tonight?'},
                    {from: 'Rauf',msg: 'Would u like to hangout?'},
                    {from: 'me',msg: 'Depends on how interesting is your plan'},
                    {from: 'me',msg: 'What you got in ur mind?'},
                ]
            },
            Asif: {
                info:{
                    pics: [
                        "https://images.unsplash.com/photo-1520024146169-3240400354ae?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                    ],
                    name: "Asif",
                    age: 32,
                    distance: "4 miles away",
                    text: "bla bla bla bla bla bla"
                },
                messages:[
                    {from: 'Asif',msg: 'hello Rauf old test'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Asif',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Asif',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Asif',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf new test'},
                ]
            },
            Nick: {
                info:{
                    pics: [
                        "https://images.unsplash.com/photo-1520024146169-3240400354ae?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                    ],
                    name: "Nick",
                    age: 32,
                    distance: "4 miles away",
                    text: "bla bla bla bla bla bla"
                },
                messages:[
                    {from: 'Nick',msg: 'hello Rauf old test'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Nick',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Nick',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf'},
                    {from: 'Nick',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf new test'},
                ]
            },
            Slava: {
                info:{
                    pics: [
                        "https://images.unsplash.com/photo-1520024146169-3240400354ae?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60"
                    ],
                    name: "Slava",
                    age: 32,
                    distance: "4 miles away",
                    text: "bla bla bla bla bla bla"
                },
                messages:[
                    {from: 'me',msg: 'hello Rauf old test'},
                    {from: 'Slava',msg: 'hello Rauf'},
                    {from: 'Slava',msg: 'hello Rauf'},
                    {from: 'Slava',msg: 'hello Rauf'},
                    {from: 'me',msg: 'hello Rauf new test'},
                ]
            },
        };
        let input=document.querySelector("#optional-input");
        return (
            <div className={"message-box-app"}>
                <ChatAppContainer data={data}/>
                <div className={"msg-container"}>
                    <form action="" className={"msg-container__form"}>
                        <input type="text" id="optional-input" className={"msg-container__input"}/>
                        <input type="submit" value={"Send"} className={"msg-container__button"}/>
                    </form>
                </div>
                {/*<MessageInput />*/}
            </div>
        );
    }
}

export default Message;